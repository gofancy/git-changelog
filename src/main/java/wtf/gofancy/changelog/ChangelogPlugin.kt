/*
 * This file is part of Git Changelog, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.changelog

import org.eclipse.jgit.api.Git
import org.eclipse.jgit.lib.ObjectId
import org.eclipse.jgit.lib.Ref
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.revwalk.RevCommit
import org.gradle.api.Plugin
import org.gradle.api.Project
import javax.inject.Inject

class ChangelogPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        target.extensions.create("changelog", ChangelogExtension::class.java)
    }
}

open class ChangelogExtension @Inject constructor(private val project: Project) {
    private val git: Git = Git.open(project.rootDir)
    private val log: List<RevCommit> = git.log().call().toList() // Get all commits in the current tree
    private val head: RevCommit = log.first() // Latest branch commit (head)
    private val tail: RevCommit = log.last() // First branch commit (tail)
    // Get all tag refs and map them to the commit they belong to
    private val tags: Map<RevCommit, Ref> = git.tagList().call()
        .associateBy { tag -> git.repository.parseCommit(tag.objectId) }
        .filterKeys(log::contains) // Remove all tags that are not in the current tree
    // Find all major.minor tags and sort them by their commit's date, descending
    private val repoTags: List<ChangelogTag> = tags.asSequence()
        .map { (commit, ref) -> ChangelogTag(Repository.shortenRefName(ref.name), commit, ref.objectId) }
        .filter { tag -> tag.name.split(".").size == 2 }
        .sortedWith(compareByDescending { tag -> tag.commit.authorIdent.`when` })
        .toList()

    /**
     * Generate a version string based on the latest tag in the git repository.
     * 
     * If the current commit is tagged, that tag is used as the version,
     * overriding any other rules. If that tag only contains a major and minor version,
     * 0 is appened as the patch version.
     * 
     * Otherwise, grab the latest existing tag, appending the commit distance as the patch version.
     * 
     * @return The generated version string
     */
    fun getVersionFromTag(): String {
        val latestTag = repoTags.first()
        // If this commit belongs to a significant version tag, append a patch number 0 for consistency
        return if (latestTag.commit == head) {
            if (latestTag.name.contains('-')) latestTag.name
            else "${latestTag.name}.0"
        }
        else {
            val distance = git.log().addRange(latestTag.commit, head).call().count()
            "${latestTag.name}.$distance"
        }
    }

    /**
     * Generate a changelog based on git commit information.
     *
     * Versions are represented by tags, where `MAJOR.MINOR` are significant and `MAJOR.MINOR.PATCH` are insignificant
     * version types.
     *
     * Changes are grouped by significant versions. Insignificant versions' commit messages are then ordered
     * by their commit date descending, and listed inside the group.
     *
     * Example default output:
     * ```
     * 0.1
     * ======
     * - 0.1.2 cleanup
     * - 0.1.1 some changes
     * - 0.1.0 release 0.1
     *
     * 0.0
     * ======
     * - 0.0.1 add code
     * - 0.0.0 initial commit
     * ```
     *
     * @param depth the amount of significant verisons to include, counting from the latest downwards. Set to 0 to ignore.
     * @param markdown enable Markdown format compatibility
     * @param includeStart include changes before the first significant version
     * @return the generated changelog string
     */
    fun generateChangelog(depth: Int = 0, markdown: Boolean = false, includeStart: Boolean = true): String {
        try {
            val changelog: StringBuilder = StringBuilder()
            val changelogTags: List<ChangelogTag> = repoTags
                .let { list -> if (includeStart) list + ChangelogTag("0.0", tail, tail) else list } // Add first branch commit
                .let { list -> if (depth > 0) list.take(depth) else list }
            // Zip each version tag with the commits between it and the next tag (or HEAD)
            val changelogCommits: List<Pair<ChangelogTag, List<RevCommit>>> = changelogTags
                .mapIndexed { index, tag ->
                    val previous =
                        if (index == 0) head
                        else git.repository.parseCommit(changelogTags[index - 1].tagId).getParent(0)
                    val tagLog = git.log().run {
                        if (tag.commit.parentCount > 0) not(tag.commit.parents.last()) // Tag commit parent
                        add(tag.commit) // Tag commit
                        add(previous) // Previous tag commit parent or head
                        call().toList()
                    }
                    Pair(tag, tagLog)
                }
            // Get max tag string length
            val maxLength: Int = changelogCommits.maxOf { (tag, log) ->
                "${tag.name}.${log.size}".length
            }
            val padString = if (markdown) "&nbsp;" else " "
            changelogCommits
                .forEach { (tag, commits) ->
                    // Append header
                    changelog.appendLine(tag.name)
                    changelog.appendLine("======")

                    // Append insignificant versions
                    commits.forEachIndexed { index, commit ->
                        val tagName: String =
                            // If this commit belongs to a significant version tag, append a patch number 0 for consistency
                            if (index == commits.lastIndex) "${tag.name}.0"
                            else tags[commit]
                                // Find the insignificant version tag name
                                ?.let { Repository.shortenRefName(it.name) }
                            // If this commit isn't tagged, calculate the version
                                ?: "${tag.name}.${commits.lastIndex - index}"
                        val padSize: Int = maxLength - tagName.length
                        val newLinePad: String = padString.repeat(4 + tagName.length + padSize)
                        commit.fullMessage.lines()
                            .filterNot(String::isEmpty)
                            .forEachIndexed { idx, line ->
                                // Append first commit message line
                                if (idx == 0) {
                                    val leadingPad: String = padString.repeat(padSize)
                                    changelog.append(" - $tagName$leadingPad $line")
                                }
                                // Append the rest of the lines offset with leading spaces
                                else {
                                    changelog.append("$newLinePad$line")
                                }
                                if (markdown) changelog.append("  ")
                                changelog.appendLine()
                            }
                    }
                    // Append new line to separate groups
                    changelog.appendLine()
                }
            return changelog.toString()
        } catch (e: Exception) {
            project.logger.error("Error generating changelog", e)
        }
        return ""
    }
}

data class ChangelogTag(val name: String, val commit: RevCommit, val tagId: ObjectId)