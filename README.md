# Git Changelog

Generate a changelog using Git commit information.

## Usage

### Installation

First, add the gofancy maven repository to `settings.gradle.kts`:
```kotlin
pluginManagement {
    repositories {
        maven {
            name = "Garden of Fancy"
            url = uri("https://maven.gofancy.wtf/releases")
        }
    }
}
```

Then apply the plugin inside the Plugins block in your `build.gradle.kts`:
```kotlin
plugins {
    id("wtf.gofancy.git-changelog") version "<version>"
}
```

### Changelog generation

Generate the changelog string using the `Project#generateChangelog` extension function.
See the javadoc for usage details.