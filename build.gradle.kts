import fr.brouillard.oss.jgitver.GitVersionCalculator
import fr.brouillard.oss.jgitver.Strategies
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.Calendar

buildscript {
    dependencies {
        classpath(group = "fr.brouillard.oss", name = "jgitver", version = "0.14.0")
    }
}

plugins {
    `java-gradle-plugin`
    `maven-publish`
    kotlin("jvm") version "1.7.10"
    id("org.cadixdev.licenser") version "0.6.1"
    id("com.gradle.plugin-publish") version "0.21.0"
}

group = "wtf.gofancy"
version = getGitVersion()

pluginBundle {
    website = "https://gitlab.com/gofancy/changelog"
    vcsUrl = "https://gitlab.com/gofancy/changelog"
    tags = listOf("git", "changelog")
}

gradlePlugin {
    plugins {
        create("changelog") {
            id = "wtf.gofancy.git-changelog"
            displayName = "Git Changelog"
            description = "A Gradle plugin for generating a changelog based on git commit information"
            implementationClass = "wtf.gofancy.changelog.ChangelogPlugin"
        }
    }
}

java {
    withSourcesJar()

    toolchain.languageVersion.set(JavaLanguageVersion.of(JavaVersion.VERSION_1_8.majorVersion))
}

license {
    header(project.file("NOTICE"))

    ext["year"] = Calendar.getInstance().get(Calendar.YEAR)
    ext["name"] = "Garden of Fancy"
    ext["app"] = "Git Changelog"
    
    include("**/**.kt")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(group = "org.eclipse.jgit", name = "org.eclipse.jgit", version = "5.13.1.202206130422-r")
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_1_8.toString()
        }
    }

    withType<Jar> {
        manifest {
            attributes(
                    "Name" to "wtf/gofancy/changelog",
                    "Specification-Title" to "Git Changelog",
                    "Specification-Version" to project.version,
                    "Specification-Vendor" to "Garden of Fancy",
                    "Implementation-Title" to "wtf.gofancy.changelog",
                    "Implementation-Version" to project.version,
                    "Implementation-Vendor" to "Garden of Fancy",
                    "Implementation-Timestamp" to DateTimeFormatter.ISO_INSTANT.format(Instant.now())
            )
        }
    }
}

publishing {
    repositories {
        val mavenUser = System.getenv("GOFANCY_MAVEN_USER")
        val mavenToken = System.getenv("GOFANCY_MAVEN_TOKEN")
        
        if (mavenUser != null && mavenToken != null) {
            maven {
                name = "gofancy"
                url = uri("https://maven.gofancy.wtf/releases")

                credentials {
                    username = mavenUser
                    password = mavenToken
                }
            }
        }
    }
}

fun getGitVersion(): String {
    val jgitver = GitVersionCalculator.location(rootDir)
        .setNonQualifierBranches("master")
        .setStrategy(Strategies.SCRIPT)
        .setScript("print \"\${metadata.CURRENT_VERSION_MAJOR};\${metadata.CURRENT_VERSION_MINOR};\${metadata.CURRENT_VERSION_PATCH + metadata.COMMIT_DISTANCE}\"")
    return jgitver.version
}
